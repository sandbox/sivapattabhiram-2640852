<?php

/**
 * Implements hook_drush_command().
 */
function twitterator_drush_command() {
  $items = array();
  $items['twitterator-update'] = array(
    'description' => t('Update Twitter Channels'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'options' => array(
      'cid' => array(
        'description' => 'The numberic ID of the channel you wish to process. Omit to process all channels.',
      ),
      'all' => array(
        'description' => 'Update all available Twitter channels.',
      ),
    ),
    'examples' => array(
      'twitterator-update --all' => 'Update the tweets for all configured channels.',
      'twitterator-update --cid=1' => 'Update the tweets for channel ID 1',
    ),
  );
  return $items;
}

/**
 * Update Tweets
 *
 * Drush command to fetch all tweets from all channels or just the one specified.
 *
 * Implements drush_HOOK_COMMAND().
 */
function drush_twitterator_update() {
  $cid = drush_get_option('cid');
  $all = drush_get_option('all');

  if (!empty($cid)) {
    twitterator_process_channel($cid);
    return;
  }

  // get all the channels
  $query = db_select('node', 'n')
           ->fields('n', array('nid', 'title', 'created'));
  $db_or = db_or()
           ->condition('type', 'twitter_channel')
           ->condition('type', 'youtube_channel')
           ->condition('type', 'rss_channel');
  $result = $query->condition($db_or)
            ->condition('status', 1)
            ->orderBy('title', 'ASC')
            ->execute();

  if (!empty($all)) {
    // process all the channels
    while ($cdata = $result->fetchObject()) {
       twitterator_process_channel($cdata->nid);
    }
  }
  else {
    // present a list of channels for the user to choose
    $option = array();
    while ($cdata = $result->fetchObject()) {
      $option[$cdata->nid] = $cdata->title . "({$cdata->nid})";
    }

    $choice = drush_choice($option, dt('Which channel would you like to input?'));
    if ($choice && array_key_exists($choice, $option)) {
      twitterator_process_channel((int)$choice);
    }
    else {
      drush_print('Unable to process channel. Illegal channel id specified.');
    }
  }
}