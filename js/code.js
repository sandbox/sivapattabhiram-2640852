
jQuery(document).ready(function() {
  jQuery('.throbber').hide();
  jQuery(".publish-node").click(function() {
     var id = jQuery(this).attr('id');
     id = id.replace('node-pub-', ''); 
     jQuery.ajax({
        type: "POST",
        url: location.protocol + '//' + location.host + Drupal.settings.basePath + "publish/node/",
        data: {'nid' : id},
        dataType: 'json',
        beforeSend: function() {
            jQuery('#throbber-'+id).show();
        },
        complete: function(){
            jQuery('#throbber-'+id).hide();
        },
        success: function(response) {
            // a successful record match; now populate the form fields
            if (response.message != '') {
                jQuery('#publish-message-'+id).text(response.message);
            }
            else {
            	jQuery('#publish-message-'+id).text("No response.");
            }
        },
        error: function(response) {
            // an error; silently log to the console in JavaScript
            console.log(response.message);
            return false;
        },
    })
})
});
