<?php
/**
 * @file
 * twitterator_features.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function twitterator_features_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function twitterator_features_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function twitterator_features_node_info() {
  $items = array(
    'rss_channel' => array(
      'name' => t('RSS Channel'),
      'base' => 'node_content',
      'description' => t('A RSS Channel will retrieve rss feeds, filter it and create Feed Item nodes.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'rss_item' => array(
      'name' => t('RSS Item'),
      'base' => 'node_content',
      'description' => t('A RSS item content type.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'tweet' => array(
      'name' => t('Tweet'),
      'base' => 'node_content',
      'description' => t('A tweet item'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'twitter_channel' => array(
      'name' => t('Twitter Channel'),
      'base' => 'node_content',
      'description' => t('A Twitter Channel will retrieve tweets from Twitter, filter it, create Tweet nodes and also retweet it to a Twitter timeline or collection.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'youtube_channel' => array(
      'name' => t('Youtube Channel'),
      'base' => 'node_content',
      'description' => t('A Youtube Channel will retrieve videos from Youtube, filter it and create Youtube Video nodes.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'youtube_video' => array(
      'name' => t('Youtube Video'),
      'base' => 'node_content',
      'description' => t('A Youtube video content type.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
