<?php
/**
 * @file
 * twitterator_features.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function twitterator_features_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_rc_channel_filters|node|rss_channel|form';
  $field_group->group_name = 'group_rc_channel_filters';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'rss_channel';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Filters',
    'weight' => '3',
    'children' => array(
      0 => 'field_rc_time_threshold',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-rc-channel-filters field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_rc_channel_filters|node|rss_channel|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_rc_publish|node|rss_channel|form';
  $field_group->group_name = 'group_rc_publish';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'rss_channel';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Publish',
    'weight' => '8',
    'children' => array(
      0 => 'field_rc_tags',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-rc-publish field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_rc_publish|node|rss_channel|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_tc_channel_filters|node|twitter_channel|form';
  $field_group->group_name = 'group_tc_channel_filters';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'twitter_channel';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Filters',
    'weight' => '3',
    'children' => array(
      0 => 'field_tc_time_threshold',
      1 => 'field_tc_ignore_retweets',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Filters',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-tc-channel-filters field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_tc_channel_filters|node|twitter_channel|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_tc_publish|node|twitter_channel|form';
  $field_group->group_name = 'group_tc_publish';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'twitter_channel';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Publish',
    'weight' => '4',
    'children' => array(
      0 => 'field_tc_channel_throttle',
      1 => 'field_tc_source_throttle',
      2 => 'field_tc_auto_publish',
      3 => 'field_tc_publish_threshold',
      4 => 'field_tc_tags',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-tc-publish field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_tc_publish|node|twitter_channel|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_tc_retweet|node|twitter_channel|form';
  $field_group->group_name = 'group_tc_retweet';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'twitter_channel';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Retweet',
    'weight' => '5',
    'children' => array(
      0 => 'field_tc_retweet_target',
      1 => 'field_tc_collection_id',
      2 => 'field_tc_retweet_mode',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-tc-retweet field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_tc_retweet|node|twitter_channel|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_yc_channel_filters|node|youtube_channel|form';
  $field_group->group_name = 'group_yc_channel_filters';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'youtube_channel';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Filters',
    'weight' => '0',
    'children' => array(
      0 => 'field_yc_time_threshold',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-yc-channel-filters field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_yc_channel_filters|node|youtube_channel|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_yc_publish|node|youtube_channel|form';
  $field_group->group_name = 'group_yc_publish';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'youtube_channel';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Publish',
    'weight' => '1',
    'children' => array(
      0 => 'field_yc_tags',
      1 => 'field_yc_auto_publish',
      2 => 'field_yc_publish_threshold',
      3 => 'field_yc_channel_throttle',
      4 => 'field_yc_source_throttle',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-yc-publish field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_yc_publish|node|youtube_channel|form'] = $field_group;

  return $export;
}
