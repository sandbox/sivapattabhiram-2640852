<?php
/**
 * @file
 * twitterator_features.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function twitterator_features_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'channel_tweets';
  $view->description = 'A view that lists tweets from a Twitter channel';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Channel Tweets';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Tweets';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['links'] = FALSE;
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_twitter_channel_target_id']['id'] = 'field_twitter_channel_target_id';
  $handler->display->display_options['relationships']['field_twitter_channel_target_id']['table'] = 'field_data_field_twitter_channel';
  $handler->display->display_options['relationships']['field_twitter_channel_target_id']['field'] = 'field_twitter_channel_target_id';
  $handler->display->display_options['relationships']['field_twitter_channel_target_id']['required'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Tweet Text */
  $handler->display->display_options['fields']['field_tweet_text']['id'] = 'field_tweet_text';
  $handler->display->display_options['fields']['field_tweet_text']['table'] = 'field_data_field_tweet_text';
  $handler->display->display_options['fields']['field_tweet_text']['field'] = 'field_tweet_text';
  $handler->display->display_options['fields']['field_tweet_text']['label'] = '';
  $handler->display->display_options['fields']['field_tweet_text']['element_label_colon'] = FALSE;
  /* Field: Content: User Link */
  $handler->display->display_options['fields']['field_tweet_user_link']['id'] = 'field_tweet_user_link';
  $handler->display->display_options['fields']['field_tweet_user_link']['table'] = 'field_data_field_tweet_user_link';
  $handler->display->display_options['fields']['field_tweet_user_link']['field'] = 'field_tweet_user_link';
  $handler->display->display_options['fields']['field_tweet_user_link']['label'] = '';
  $handler->display->display_options['fields']['field_tweet_user_link']['element_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_user_link']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_user_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_tweet_user_link']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_user_link']['element_default_classes'] = FALSE;
  /* Field: Content: User Image */
  $handler->display->display_options['fields']['field_tweet_user_image']['id'] = 'field_tweet_user_image';
  $handler->display->display_options['fields']['field_tweet_user_image']['table'] = 'field_data_field_tweet_user_image';
  $handler->display->display_options['fields']['field_tweet_user_image']['field'] = 'field_tweet_user_image';
  $handler->display->display_options['fields']['field_tweet_user_image']['label'] = '';
  $handler->display->display_options['fields']['field_tweet_user_image']['element_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_user_image']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_user_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_tweet_user_image']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_user_image']['element_default_classes'] = FALSE;
  /* Field: Content: User Name */
  $handler->display->display_options['fields']['field_tweet_user_name']['id'] = 'field_tweet_user_name';
  $handler->display->display_options['fields']['field_tweet_user_name']['table'] = 'field_data_field_tweet_user_name';
  $handler->display->display_options['fields']['field_tweet_user_name']['field'] = 'field_tweet_user_name';
  $handler->display->display_options['fields']['field_tweet_user_name']['label'] = '';
  $handler->display->display_options['fields']['field_tweet_user_name']['element_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_user_name']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_user_name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_tweet_user_name']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_user_name']['element_default_classes'] = FALSE;
  /* Field: Content: Screen Name */
  $handler->display->display_options['fields']['field_tweet_screen_name']['id'] = 'field_tweet_screen_name';
  $handler->display->display_options['fields']['field_tweet_screen_name']['table'] = 'field_data_field_tweet_screen_name';
  $handler->display->display_options['fields']['field_tweet_screen_name']['field'] = 'field_tweet_screen_name';
  $handler->display->display_options['fields']['field_tweet_screen_name']['label'] = '';
  $handler->display->display_options['fields']['field_tweet_screen_name']['element_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_screen_name']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_screen_name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_tweet_screen_name']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_screen_name']['element_default_classes'] = FALSE;
  /* Field: Content: Media URL */
  $handler->display->display_options['fields']['field_tweet_media_url']['id'] = 'field_tweet_media_url';
  $handler->display->display_options['fields']['field_tweet_media_url']['table'] = 'field_data_field_tweet_media_url';
  $handler->display->display_options['fields']['field_tweet_media_url']['field'] = 'field_tweet_media_url';
  $handler->display->display_options['fields']['field_tweet_media_url']['label'] = '';
  $handler->display->display_options['fields']['field_tweet_media_url']['element_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_media_url']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_media_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_tweet_media_url']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_media_url']['element_default_classes'] = FALSE;
  /* Field: Content: Media Expanded URL */
  $handler->display->display_options['fields']['field_tweet_media_expanded_url']['id'] = 'field_tweet_media_expanded_url';
  $handler->display->display_options['fields']['field_tweet_media_expanded_url']['table'] = 'field_data_field_tweet_media_expanded_url';
  $handler->display->display_options['fields']['field_tweet_media_expanded_url']['field'] = 'field_tweet_media_expanded_url';
  $handler->display->display_options['fields']['field_tweet_media_expanded_url']['label'] = '';
  $handler->display->display_options['fields']['field_tweet_media_expanded_url']['element_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_media_expanded_url']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_media_expanded_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_tweet_media_expanded_url']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_media_expanded_url']['element_default_classes'] = FALSE;
  /* Field: Content: Tweet ID */
  $handler->display->display_options['fields']['field_tweet_id']['id'] = 'field_tweet_id';
  $handler->display->display_options['fields']['field_tweet_id']['table'] = 'field_data_field_tweet_id';
  $handler->display->display_options['fields']['field_tweet_id']['field'] = 'field_tweet_id';
  $handler->display->display_options['fields']['field_tweet_id']['label'] = '';
  $handler->display->display_options['fields']['field_tweet_id']['element_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_id']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_id']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_tweet_id']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_id']['element_default_classes'] = FALSE;
  /* Field: Content: Creation Date */
  $handler->display->display_options['fields']['field_tweet_creation_date']['id'] = 'field_tweet_creation_date';
  $handler->display->display_options['fields']['field_tweet_creation_date']['table'] = 'field_data_field_tweet_creation_date';
  $handler->display->display_options['fields']['field_tweet_creation_date']['field'] = 'field_tweet_creation_date';
  $handler->display->display_options['fields']['field_tweet_creation_date']['label'] = '';
  $handler->display->display_options['fields']['field_tweet_creation_date']['element_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_creation_date']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_creation_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_tweet_creation_date']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_creation_date']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_tweet_creation_date']['type'] = 'number_unformatted';
  $handler->display->display_options['fields']['field_tweet_creation_date']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 0,
  );
  /* Field: Content: Retweet Count */
  $handler->display->display_options['fields']['field_tweet_retweet_count']['id'] = 'field_tweet_retweet_count';
  $handler->display->display_options['fields']['field_tweet_retweet_count']['table'] = 'field_data_field_tweet_retweet_count';
  $handler->display->display_options['fields']['field_tweet_retweet_count']['field'] = 'field_tweet_retweet_count';
  $handler->display->display_options['fields']['field_tweet_retweet_count']['label'] = '';
  $handler->display->display_options['fields']['field_tweet_retweet_count']['element_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_retweet_count']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_retweet_count']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_tweet_retweet_count']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_retweet_count']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_tweet_retweet_count']['type'] = 'number_unformatted';
  $handler->display->display_options['fields']['field_tweet_retweet_count']['settings'] = array(
    'thousand_separator' => ' ',
    'prefix_suffix' => 1,
  );
  /* Field: Content: Favorite Count */
  $handler->display->display_options['fields']['field_tweet_favorite_count']['id'] = 'field_tweet_favorite_count';
  $handler->display->display_options['fields']['field_tweet_favorite_count']['table'] = 'field_data_field_tweet_favorite_count';
  $handler->display->display_options['fields']['field_tweet_favorite_count']['field'] = 'field_tweet_favorite_count';
  $handler->display->display_options['fields']['field_tweet_favorite_count']['label'] = '';
  $handler->display->display_options['fields']['field_tweet_favorite_count']['element_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_favorite_count']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_favorite_count']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_tweet_favorite_count']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_favorite_count']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_tweet_favorite_count']['type'] = 'number_unformatted';
  $handler->display->display_options['fields']['field_tweet_favorite_count']['settings'] = array(
    'thousand_separator' => ' ',
    'prefix_suffix' => 1,
  );
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['relationship'] = 'field_twitter_channel_target_id';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'raw';
  $handler->display->display_options['arguments']['nid']['default_argument_options']['index'] = '1';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'tweet' => 'tweet',
  );

  /* Display: Tweets */
  $handler = $view->new_display('page', 'Tweets', 'page_2');
  $handler->display->display_options['path'] = 'tweets/%';

  /* Display: All */
  $handler = $view->new_display('page', 'All', 'page_3');
  $handler->display->display_options['path'] = 'tweets/%/all';
  $handler->display->display_options['menu']['type'] = 'default tab';
  $handler->display->display_options['menu']['title'] = 'All Tweets';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $handler->display->display_options['tab_options']['weight'] = '0';

  /* Display: Published */
  $handler = $view->new_display('page', 'Published', 'page');
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'tweet' => 'tweet',
  );
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  $handler->display->display_options['path'] = 'tweets/%/published';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Published';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Review */
  $handler = $view->new_display('page', 'Review', 'page_1');
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'tweet' => 'tweet',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '0';
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  $handler->display->display_options['path'] = 'tweets/%/review';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Review';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['channel_tweets'] = $view;

  $view = new view();
  $view->name = 'channel_videos';
  $view->description = 'A view that lists tweets from a Twitter channel';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Channel Videos';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Videos';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['links'] = FALSE;
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_youtube_channel_target_id']['id'] = 'field_youtube_channel_target_id';
  $handler->display->display_options['relationships']['field_youtube_channel_target_id']['table'] = 'field_data_field_youtube_channel';
  $handler->display->display_options['relationships']['field_youtube_channel_target_id']['field'] = 'field_youtube_channel_target_id';
  $handler->display->display_options['relationships']['field_youtube_channel_target_id']['required'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Tweet Text */
  $handler->display->display_options['fields']['field_tweet_text']['id'] = 'field_tweet_text';
  $handler->display->display_options['fields']['field_tweet_text']['table'] = 'field_data_field_tweet_text';
  $handler->display->display_options['fields']['field_tweet_text']['field'] = 'field_tweet_text';
  $handler->display->display_options['fields']['field_tweet_text']['label'] = '';
  $handler->display->display_options['fields']['field_tweet_text']['element_label_colon'] = FALSE;
  /* Field: Content: User Link */
  $handler->display->display_options['fields']['field_tweet_user_link']['id'] = 'field_tweet_user_link';
  $handler->display->display_options['fields']['field_tweet_user_link']['table'] = 'field_data_field_tweet_user_link';
  $handler->display->display_options['fields']['field_tweet_user_link']['field'] = 'field_tweet_user_link';
  $handler->display->display_options['fields']['field_tweet_user_link']['label'] = '';
  $handler->display->display_options['fields']['field_tweet_user_link']['element_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_user_link']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_user_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_tweet_user_link']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_user_link']['element_default_classes'] = FALSE;
  /* Field: Content: User Image */
  $handler->display->display_options['fields']['field_tweet_user_image']['id'] = 'field_tweet_user_image';
  $handler->display->display_options['fields']['field_tweet_user_image']['table'] = 'field_data_field_tweet_user_image';
  $handler->display->display_options['fields']['field_tweet_user_image']['field'] = 'field_tweet_user_image';
  $handler->display->display_options['fields']['field_tweet_user_image']['label'] = '';
  $handler->display->display_options['fields']['field_tweet_user_image']['element_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_user_image']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_user_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_tweet_user_image']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_user_image']['element_default_classes'] = FALSE;
  /* Field: Content: User Name */
  $handler->display->display_options['fields']['field_tweet_user_name']['id'] = 'field_tweet_user_name';
  $handler->display->display_options['fields']['field_tweet_user_name']['table'] = 'field_data_field_tweet_user_name';
  $handler->display->display_options['fields']['field_tweet_user_name']['field'] = 'field_tweet_user_name';
  $handler->display->display_options['fields']['field_tweet_user_name']['label'] = '';
  $handler->display->display_options['fields']['field_tweet_user_name']['element_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_user_name']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_user_name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_tweet_user_name']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_user_name']['element_default_classes'] = FALSE;
  /* Field: Content: Screen Name */
  $handler->display->display_options['fields']['field_tweet_screen_name']['id'] = 'field_tweet_screen_name';
  $handler->display->display_options['fields']['field_tweet_screen_name']['table'] = 'field_data_field_tweet_screen_name';
  $handler->display->display_options['fields']['field_tweet_screen_name']['field'] = 'field_tweet_screen_name';
  $handler->display->display_options['fields']['field_tweet_screen_name']['label'] = '';
  $handler->display->display_options['fields']['field_tweet_screen_name']['element_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_screen_name']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_screen_name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_tweet_screen_name']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_screen_name']['element_default_classes'] = FALSE;
  /* Field: Content: Media URL */
  $handler->display->display_options['fields']['field_tweet_media_url']['id'] = 'field_tweet_media_url';
  $handler->display->display_options['fields']['field_tweet_media_url']['table'] = 'field_data_field_tweet_media_url';
  $handler->display->display_options['fields']['field_tweet_media_url']['field'] = 'field_tweet_media_url';
  $handler->display->display_options['fields']['field_tweet_media_url']['label'] = '';
  $handler->display->display_options['fields']['field_tweet_media_url']['element_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_media_url']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_media_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_tweet_media_url']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_media_url']['element_default_classes'] = FALSE;
  /* Field: Content: Media Expanded URL */
  $handler->display->display_options['fields']['field_tweet_media_expanded_url']['id'] = 'field_tweet_media_expanded_url';
  $handler->display->display_options['fields']['field_tweet_media_expanded_url']['table'] = 'field_data_field_tweet_media_expanded_url';
  $handler->display->display_options['fields']['field_tweet_media_expanded_url']['field'] = 'field_tweet_media_expanded_url';
  $handler->display->display_options['fields']['field_tweet_media_expanded_url']['label'] = '';
  $handler->display->display_options['fields']['field_tweet_media_expanded_url']['element_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_media_expanded_url']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_media_expanded_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_tweet_media_expanded_url']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_media_expanded_url']['element_default_classes'] = FALSE;
  /* Field: Content: Tweet ID */
  $handler->display->display_options['fields']['field_tweet_id']['id'] = 'field_tweet_id';
  $handler->display->display_options['fields']['field_tweet_id']['table'] = 'field_data_field_tweet_id';
  $handler->display->display_options['fields']['field_tweet_id']['field'] = 'field_tweet_id';
  $handler->display->display_options['fields']['field_tweet_id']['label'] = '';
  $handler->display->display_options['fields']['field_tweet_id']['element_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_id']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_id']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_tweet_id']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_id']['element_default_classes'] = FALSE;
  /* Field: Content: Creation Date */
  $handler->display->display_options['fields']['field_tweet_creation_date']['id'] = 'field_tweet_creation_date';
  $handler->display->display_options['fields']['field_tweet_creation_date']['table'] = 'field_data_field_tweet_creation_date';
  $handler->display->display_options['fields']['field_tweet_creation_date']['field'] = 'field_tweet_creation_date';
  $handler->display->display_options['fields']['field_tweet_creation_date']['label'] = '';
  $handler->display->display_options['fields']['field_tweet_creation_date']['element_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_creation_date']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_creation_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_tweet_creation_date']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_creation_date']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_tweet_creation_date']['type'] = 'number_unformatted';
  $handler->display->display_options['fields']['field_tweet_creation_date']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 0,
  );
  /* Field: Content: Retweet Count */
  $handler->display->display_options['fields']['field_tweet_retweet_count']['id'] = 'field_tweet_retweet_count';
  $handler->display->display_options['fields']['field_tweet_retweet_count']['table'] = 'field_data_field_tweet_retweet_count';
  $handler->display->display_options['fields']['field_tweet_retweet_count']['field'] = 'field_tweet_retweet_count';
  $handler->display->display_options['fields']['field_tweet_retweet_count']['label'] = '';
  $handler->display->display_options['fields']['field_tweet_retweet_count']['element_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_retweet_count']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_retweet_count']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_tweet_retweet_count']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_retweet_count']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_tweet_retweet_count']['type'] = 'number_unformatted';
  $handler->display->display_options['fields']['field_tweet_retweet_count']['settings'] = array(
    'thousand_separator' => ' ',
    'prefix_suffix' => 1,
  );
  /* Field: Content: Favorite Count */
  $handler->display->display_options['fields']['field_tweet_favorite_count']['id'] = 'field_tweet_favorite_count';
  $handler->display->display_options['fields']['field_tweet_favorite_count']['table'] = 'field_data_field_tweet_favorite_count';
  $handler->display->display_options['fields']['field_tweet_favorite_count']['field'] = 'field_tweet_favorite_count';
  $handler->display->display_options['fields']['field_tweet_favorite_count']['label'] = '';
  $handler->display->display_options['fields']['field_tweet_favorite_count']['element_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_favorite_count']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_favorite_count']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_tweet_favorite_count']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_tweet_favorite_count']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_tweet_favorite_count']['type'] = 'number_unformatted';
  $handler->display->display_options['fields']['field_tweet_favorite_count']['settings'] = array(
    'thousand_separator' => ' ',
    'prefix_suffix' => 1,
  );
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['relationship'] = 'field_youtube_channel_target_id';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'raw';
  $handler->display->display_options['arguments']['nid']['default_argument_options']['index'] = '1';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'youtube_video' => 'youtube_video',
  );

  /* Display: Videos */
  $handler = $view->new_display('page', 'Videos', 'page_2');
  $handler->display->display_options['path'] = 'videos/%';

  /* Display: All */
  $handler = $view->new_display('page', 'All', 'page_3');
  $handler->display->display_options['path'] = 'videos/%/all';
  $handler->display->display_options['menu']['type'] = 'default tab';
  $handler->display->display_options['menu']['title'] = 'All Videos';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $handler->display->display_options['tab_options']['weight'] = '0';

  /* Display: Published */
  $handler = $view->new_display('page', 'Published', 'page');
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'youtube_video' => 'youtube_video',
  );
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  $handler->display->display_options['path'] = 'videos/%/published';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Published';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Review */
  $handler = $view->new_display('page', 'Review', 'page_1');
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'youtube_video' => 'youtube_video',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '0';
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  $handler->display->display_options['path'] = 'videos/%/review';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Review';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['channel_videos'] = $view;

  return $export;
}
