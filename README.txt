A Twitter curation module that helps you to fetch tweets from Twitter, filter it based on different conditions and optionally retweet to Twitter.
