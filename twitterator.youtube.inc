<?php

/**
 * @file
 * Youtube specific functionality.
 *
 */

/**
 * Update the channel: Fetch the videos, apply filters and execute actions.
 */
function twitterator_process_youtube_channel($channel) {
  TweetUtils::debug("Processing channel {$channel->title}");
  $lang = $channel->language;
  $channel_id = $channel->nid;

  // get the time threshold
  $time_threshold = $channel->field_yc_time_threshold[$lang][0]['value'];
  if (empty($time_threshold)) {
    $time_threshold = variable_get('twitterator_time_threshold', 0);
  }

   $channel_videos = array();

  // get all the sources
  $sources = $channel->field_yc_source[$lang];
  foreach ($sources as $source) {
    $source_item = entity_load_single('field_collection_item', $source['value']);
    $youtube_channel_id = $source_item->field_yc_source_id[$lang][0]['value'];

    $popular_threshold = $source_item->field_yc_popular_threshold[$lang][0]['value'];

    $videos = twitterator_fetch_videos($youtube_channel_id);

    if (empty($videos)) continue;

    // apply the time and popular thresholds
    foreach($videos as $video) {
      $created = $video['published_timestamp'];
      if (!empty($time_threshold) && $created < (time() - 60 * $time_threshold))
        continue;

      module_invoke_all('process_video', $video);

      $popular_count = $video['view_count'];
      if (!empty($popular_threshold) && $popular_count < $popular_threshold)
        continue;

      if (sizeof(module_implements('filter_video')) > 0) {
        $statuses = module_invoke_all('filter_video', $video);
        if (in_array(1, $statuses)) continue;
      }

      //TweetUtils::debug("Processing {$tweet->id_str} with a popular count of $popular_count");
      $weight = empty($popular_threshold) ? $popular_count : $popular_count / $popular_threshold;
      $channel_videos[] = array('id' => $video['url'], 'source_id' => $source_item->item_id, 'weight' => $weight, 'video' => $video);
    }
  }

  // sort the videos based on popularity
  usort(
    $channel_videos,
    function($a, $b) {
      $result = 0;
      if ($a['weight'] < $b['weight']) {
        $result = 1;
      }
      else if ($a['weight'] > $b['weight']) {
        $result = -1;
      }
      return $result;
    }
  );

  // apply publish actions
  $publish_mode = $channel->field_yc_auto_publish[$lang][0]['value'];
  $channel_throttle = $channel->field_yc_channel_throttle[$lang][0]['value'];
  $source_throttle = $channel->field_yc_source_throttle[$lang][0]['value'];
  $videos_published = array();

  foreach ($channel_videos as $video_data) {
    $publish = ($publish_mode == 2) ? FALSE : TRUE;
    $source_id = (int)$video_data['source_id'];
    $weight = $video_data['weight'];
    $video = $video_data['video'];

    TweetUtils::debug("Processing {$video['url']} with a weight of $weight");

    if ($publish_mode == 3) {
      if (!empty($source_throttle) && !empty($videos_published[$source_id]) &&
          $videos_published[$source_id] >= $source_throttle)
        $publish = FALSE;
      $nvideos = array_sum(array_values($videos_published));
      if (!empty($channel_throttle) && $nvideos >= $channel_throttle)
        $publish = FALSE;
    }

    // check if the video is already created in the channel
    $nid = twitterator_get_video($video_data['id'], $channel_id);
    if (!empty($nid)) {
      $video_node = node_load($nid);
      if ($video_node->status == 0 && $publish) {
        $video_node->status = 1;
        module_invoke_all('publish_video', array('node' => $video_node, 'video' => $video));
        node_save($video_node);
        TweetUtils::debug("Video node '{$video_node->title}' published.");
      }
      else
        continue; // move to the next video
    }
    else {
      if (!twitterator_create_video($video, $channel, $publish))
        continue;
    }

    if ($publish) {
      $videos_published[$source_id] = empty($videos_published[$source_id]) ? 1 : $videos_published[$source_id]+1;
    }
  }
}

/**
 * Get the Videos from Youtube using RSS Feeds
 *
 * @param int channel_id
 *   The youtube channel_id - You can get the channel id by searching for the attribute
 *   data-channel-external-id in the source code of the youtube page
 * @return array videos
 *   An array of all the  videos for this source. FALSE if there are problems.
 */
function twitterator_fetch_videos($channel_id) {
  $url = "https://www.youtube.com/feeds/videos.xml?channel_id=" . $channel_id;
  return twitterator_fetch_rss_feed($url);
}

/**
 * Parse the rss feed using SimpleXML parser.
 */
function twitterator_fetch_rss_feed($url) {
  module_load_include('inc', 'twitterator', 'inc/http_request');
  module_load_include('inc', 'twitterator', 'inc/YoutubeParser');

  // fetch the feed using http_request library
  $result = http_request_get($url, NULL, NULL, NULL, 120);
  if (!in_array($result->code, array(200, 201, 202, 203, 204, 205, 206))) {
    watchdog("twitterator", 'Download of @url failed with code !code.', array('@url' => $url, '!code' => $result->code), WATCHDOG_ERROR);
    TweetUtils::debug(t('Download of @url failed with code !code.', array('@url' => $url, '!code' => $result->code)));
    return FALSE;
  }

  $parser = new YoutubeParser();
  $items = $parser->parse($result->data);

  return $items;
}

/**
 * Generate a title from a random text.
 */
function twitterator_create_title($text = FALSE) {
  // Explode to words and use the first 3 words.
  $words = preg_split("/[\s,]+/", $text);
  $words = array_slice($words, 0, 3);
  return implode(' ', $words);
}

/**
 * Fetch a video node from a channel given a video url.
 */
function twitterator_get_video($url, $channel_id) {
  $query = new EntityFieldQuery();
  $result = $query->entityCondition('entity_type', 'node')
                  ->entityCondition('bundle', 'youtube_video')
                  ->fieldCondition('field_youtube_url', 'value', $url, '=')
                  ->fieldCondition('field_youtube_channel', 'target_id', $channel_id, '=')
                  ->execute();
  if (isset($result['node'])) {
    $video_items_nids = array_keys($result['node']);
    return array_pop($video_items_nids);
  }

  return 0;
}

/**
 * Create a Video Node object from the Video RSS Object.
 * @param object $video
 *   The video object returned by the RSS feed
 * @param int channel_id
 *   The node id of the channel to which this video has to be added
 * @return int node_id
 *   The node id of the created video.
 */
function twitterator_create_video($video, $channel, $publish=TRUE) {
  // Populate our video object with the data from json
  $node = new stdClass();

  $node->type = 'youtube_video';
  $node->uid = 1;
  $node->status = $publish;
  $node->comment = 0;
  $node->promote = 0;
  $node->moderate = 0;
  $node->sticky = 0;
  $node->language = LANGUAGE_NONE;

  $node->title = $video['title'];

  $node->field_youtube_description[$node->language][0] = array(
    'value' => html_entity_decode($video['description'], ENT_QUOTES, "utf-8"),
    //'value' => utf8_encode(htmlspecialchars_decode($content)),
    'format' => 'full_html',
  );

  $node->field_youtube_url[$node->language][0]['value'] = $video['url'];

  $node->field_youtube_thumbnail[$node->language][0]['value'] = $video['thumbnail'];

  $node->field_youtube_published_date[$node->language][0]['value'] = $video['published_timestamp'];

  $node->field_youtube_source_name[$node->language][0]['value'] = $video['feed_title'];

  if (!empty($channel)) {
    $node->field_youtube_channel[$node->language][0]['target_id'] = $channel->nid;
  }

  if (!empty($channel->field_yc_tags[$channel->language][0]['tid'])) {
    $node->field_youtube_tags[$node->language][0]['tid'] = $channel->field_yc_tags[$channel->language][0]['tid'];
  }

  if (sizeof(module_implements('create_video_node')) > 0) {
    $data = array('node' => $node, 'video' => $video);
    $statuses = module_invoke_all('create_video_node', $data);
    if (in_array(0, $statuses)) {
      TweetUtils::debug("Video node: '{$node->title}' skipped.");
      return FALSE;
    }
  }

  // Save the node
  node_save($node);

  TweetUtils::debug("Video node: '{$node->title}' created.");

  return $node->nid;
}
