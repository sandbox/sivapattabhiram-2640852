<?php

/**
 * @file
 * Twitter specific functionality.
 *
 */

/**
 * Update the channel: Fetch the tweets, apply filters and execute actions.
 */
function twitterator_process_twitter_channel($channel) {
  TweetUtils::debug("Processing channel {$channel->title}");
  $lang = $channel->language;
  $channel_id = $channel->nid;

  // get the authentication details
  $account_id = $channel->field_twitter_account[$lang][0]['target_id'];
  $twitter_account = entity_load_single('twitter_account', $account_id);

  // get the time threshold
  $time_threshold = $channel->field_tc_time_threshold[$lang][0]['value'];
  if (empty($time_threshold)) {
    $time_threshold = variable_get('twitterator_time_threshold', 0);
  }

  $ignore_retweets = $channel->field_tc_ignore_retweets[$lang][0]['value'];

  $channel_tweets = array();

  // get all the sources
  $sources = $channel->field_tc_source[$lang];
  foreach ($sources as $source) {
    $source_item = entity_load_single('field_collection_item', $source['value']);
    $source_type = $source_item->field_tc_source_type[$lang][0]['value'];
    $source_detail = $source_item->field_tc_source_detail[$lang][0]['value'];

    $popular_threshold = $source_item->field_tc_popular_threshold[$lang][0]['value'];
    if (empty( $popular_threshold)) {
      $popular_threshold = variable_get('twitterator_popular_threshold', 0);
    }
    $popular_scheme = variable_get('twitterator_popular_scheme', 3);

    $tweets = twitterator_fetch_tweets($source_type, $source_detail, $twitter_account);

    if (empty($tweets)) continue;

    // apply the time and popular thresholds
    foreach($tweets as $tweet) {
      // if it is a retweet, process the original tweet
      if (!empty($tweet->retweeted_status)) {
        if ($ignore_retweets) continue;
        $tweet = $tweet->retweeted_status;
      }

      $created = strtotime($tweet->created_at);
      if (!empty($time_threshold) && $created < (time() - 60 * $time_threshold))
        continue;

      if (sizeof(module_implements('process_tweet')) > 0) {
        $tweet_item = array('channel' => $channel->title, 'feed_item' => $tweet->user->name, 'tweet' => $tweet);
        $statuses = module_invoke_all('process_tweet', $tweet_item);
        if (in_array(0, $statuses)) continue;
      }

      $popular_count = 0;
      switch ($popular_scheme) {
        case 1:
          $popular_count = $tweet->retweet_count;
          break;
        case 2:
          $popular_count = $tweet->favorite_count;
          break;
        case 3:
          $popular_count = $tweet->retweet_count + $tweet->favorite_count;
          break;
        case 4:
          $popular_count = $tweet->mentions;
          break;
      }
      if (!empty($popular_threshold) && $popular_count < $popular_threshold)
        continue;

      TweetUtils::debug("Processing {$tweet->id_str} with a popular count of $popular_count");
      $weight = empty($popular_threshold) ? $popular_count : $popular_count / $popular_threshold;
      $channel_tweets[] = array('id' => $tweet->id_str, 'source_id' => $source_item->item_id, 'weight' => $weight, 'tweet' => $tweet);
    }
  }

  // sort the tweets based on popularity
  usort(
    $channel_tweets,
    function($a, $b) {
      $result = 0;
      if ($a['weight'] < $b['weight']) {
        $result = 1;
      }
      else if ($a['weight'] > $b['weight']) {
        $result = -1;
      }
      return $result;
    }
  );

  // apply publish and retweet actions
  $publish_mode = $channel->field_tc_auto_publish[$lang][0]['value'];
  $channel_throttle = $channel->field_tc_channel_throttle[$lang][0]['value'];
  $source_throttle = $channel->field_tc_source_throttle[$lang][0]['value'];
  $retweet_mode = $channel->field_tc_retweet_mode[$lang][0]['value'];
  $tweets_published = array();

  foreach ($channel_tweets as $tweet_data) {
    $publish = ($publish_mode == 2) ? FALSE : TRUE;
    $source_id = (int)$tweet_data['source_id'];
    $weight = $tweet_data['weight'];
    $tweet = $tweet_data['tweet'];

    TweetUtils::debug("Processing {$tweet->id_str} with a weight of $weight");

    if ($publish_mode == 3) {
      if (!empty($source_throttle) && !empty($tweets_published[$source_id]) &&
          $tweets_published[$source_id] >= $source_throttle)
        $publish = FALSE;
      $ntweets = array_sum(array_values($tweets_published));
      if (!empty($channel_throttle) && $ntweets >= $channel_throttle)
        $publish = FALSE;
    }

    // check if the tweet is already created in the channel
    $nid = twitterator_get_tweet($tweet_data['id'], $channel_id);
    if (!empty($nid)) {
      $tweet_node = node_load($nid);
      if ($tweet_node->status == 0 && $publish) {
        $tweet_node->status = 1;
        module_invoke_all('publish_tweet', array('node' => $tweet_node, 'tweet' => $tweet));
        node_save($tweet_node);
        TweetUtils::debug("Twitter node '{$tweet_node->title}' published.");
      }
      else
        continue; // move to the next tweet
    }
    else {
      if (!twitterator_create_tweet($tweet, $channel, $publish))
        continue;
    }

    if ($publish) {
      $tweets_published[$source_id] = empty($tweets_published[$source_id]) ? 1 : $tweets_published[$source_id]+1;
    }

    // process channel retweet actions
    if (!empty($retweet_mode) && $retweet_mode == 1 && $publish) {
      $target = $channel->field_tc_retweet_target[$lang][0]['value'];
      switch ($target) {
        case 1:
          twitterator_retweet_tweet($tweet_data['id'], $twitter_account);
          break;
        case 2:
          $collection_id = $channel->field_tc_collection_id[$lang][0]['value'];
          if (!empty($collection_id))
           twitterator_post_to_collection($tweet_data['id'], $collection_id, $twitter_account);
      }
    }
  }
}

/**
 * Get the Tweets from Twitter using 1.1 REST apis
 *
 * @param int source_type
 *   The source type which can be TWITTER_TIMELINE, TWITTER_SEARCH, TWITTER_LIST
 * @param string source_detail
 *   User screen name, search query or list name (depending on $type parameter)
 * @param array settings
 *   OAuth settings which will be used to query the tweets
 * @return array tweets
 *   An array of all the tweets for this source. FALSE if there are problems.
 */
function twitterator_fetch_tweets($source_type, $source_detail, $twitter_account) {
  $data = array();
  $settings = twitterator_twitter_account_info($twitter_account);

  switch ($source_type) {
    case TWITTER_TIMELINE:
      $url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
      $getfield = '?screen_name=' . $source_detail;
      $getfield = $getfield . '&count=200' . '&include_rts=true' . '&exclude_replies=true';
      $requestMethod = 'GET';
      $twitter = new TwitterAPIExchange($settings);
      $results = $twitter->setGetfield($getfield)
                         ->buildOauth($url, $requestMethod)
                         ->performRequest();
      $data = json_decode($results);
      TweetUtils::debug("Processing Twitter user timeline for @$source_detail: number of tweets retrieved is " . count($data));
      break;
  }

  if (!empty($data->errors)) {
    foreach($data->errors as $error) {
      TweetUtils::debug("Twitter Fetch Tweets Failed: {$error->code} {$error->message}");
    }
    return array();
  }

  return $data;
}

/**
 * Retweet a tweet to the twitter account.
 */
function twitterator_retweet_tweet($tweet_id, $twitter_account) {
  $url = 'https://api.twitter.com/1.1/statuses/retweet/' . $tweet_id . '.json';

  // get the authentication details
  $settings = twitterator_twitter_account_info($twitter_account);

  TweetUtils::debug("Retweeting item with tweet_id $tweet_id to twitter");

  $postfields = array ();
  $requestMethod = 'POST';
  $twitter = new TwitterAPIExchange($settings);
  $results = $twitter->buildOauth($url, $requestMethod)->setPostfields($postfields)->performRequest();

  $data = json_decode($results);

  if (empty($data->retweeted_status) || $data->retweeted_status->id_str != $tweet_id) {
    TweetUtils::debug("Failed to retweet tweet $tweet_id to twitter.\r\n" . print_r($data));
    watchdog("twitterator", "Failed to retweet tweet %tweet_id to Twitter: %data", array('%tweet_id' => $tweet_id, '%data' => "<pre>".print_r($data)."</pre>"), WATCHDOG_ERROR);
  }

  return $data;
}

/**
 * Post a tweet to a Twitter collection.
 */
function twitterator_post_to_collection($tweet_id, $collection_id, $twitter_account) {
  $url = "https://api.twitter.com/1.1/collections/entries/add.json";

  // get the authentication details
  $settings = twitterator_twitter_account_info($twitter_account);

  $postfields = array ( 'id' => $collection_id, 'tweet_id' => $tweet_id);

  TweetUtils::debug("Posting tweet $tweet_id to collection $collection_id twitter");

  $requestMethod = 'POST';
  $twitter = new TwitterAPIExchange($twitter_settings);
  //$results = $twitter->buildOauth($url, $requestMethod)->setPostfields($postfields)->performRequest();
  $results = $twitter->request($url, $requestMethod, $postfields);

  $data = json_decode($results);

  if (!empty($data->response->errors)) {
    TweetUtils::debug("Failed to post tweet $tweet_id to collection $collection_id.\r\n" . print_r($data));
    watchdog("twitterator", "Failed to post tweet %tweet_id Twitter: %data", array('%tweet_id' => $tweet_id, '%data' => print_r($data)), WATCHDOG_ERROR);
  }
}

/**
 * Get the OAuth settings provided in the Twitter Account.
 */
function twitterator_twitter_account_info($twitter_account) {
  $settings = array(
    'oauth_access_token' => $twitter_account->oauth_token,
    'oauth_access_token_secret' => $twitter_account->oauth_token_secret,
    'consumer_key' => $twitter_account->consumer_key,
    'consumer_secret' => $twitter_account->consumer_secret
  );

  return $settings;
}

/**
 * Fetch a tweet node from a channel given a tweet id.
 */
function twitterator_get_tweet($tweet_id, $channel_id) {
  $query = new EntityFieldQuery();
  $result = $query->entityCondition('entity_type', 'node')
                  ->entityCondition('bundle', 'tweet')
                  ->fieldCondition('field_tweet_id', 'value', $tweet_id, '=')
                  ->fieldCondition('field_twitter_channel', 'target_id', $channel_id, '=')
                  ->execute();
  if (isset($result['node'])) {
    $tweet_items_nids = array_keys($result['node']);
    return array_pop($tweet_items_nids);
  }

  return 0;
}

/**
 * Create a Tweet Node object from the Twitter JSON tweet.
 * @param object $tweet
 *   The JSON tweet object returned by the Twitter apis
 * @param int channel_id
 *   The node id of the channel to which this tweet has to be added
 * @return int node_id
 *   The node id of the created tweet.
 */
function twitterator_create_tweet($tweet, $channel, $publish=TRUE) {
  // Populate our tweet object with the data from json
  $node = new stdClass();

  $node->type = 'tweet';
  $node->uid = 1;
  $node->status = $publish;
  $node->comment = 0;
  $node->promote = 0;
  $node->moderate = 0;
  $node->sticky = 0;
  $node->language = LANGUAGE_NONE;

  $tweet_text = TweetUtils::tweet_tidy_text($tweet->text);
  $node->title = $tweet->user->screen_name . ': ' . $tweet->id_str;

  $content = TweetUtils::tweet_extract_text($tweet);
  $node->field_tweet_text[$node->language][0] = array(
    'value' => html_entity_decode($content, ENT_COMPAT|ENT_HTML401, "utf-8"),
    //'value' => utf8_encode(htmlspecialchars_decode($content)),
    'format' => 'full_html',
  );

  $node->field_tweet_id[$node->language][0]['value'] = $tweet->id_str;

  $node->field_tweet_user_link[$node->language][0]['value'] = 'https://twitter.com/' . $tweet->user->screen_name;

  $node->field_tweet_user_image[$node->language][0]['value'] = $tweet->user->profile_image_url;

  $node->field_tweet_user_name[$node->language][0]['value'] = $tweet->user->name;

  $node->field_tweet_screen_name[$node->language][0]['value'] = $tweet->user->screen_name;

  if (isset($tweet->entities->media)) {
    $node->field_tweet_media_url[$node->language][0]['value']  = $tweet->entities->media [0]->media_url;
    $node->field_tweet_media_expanded_url[$node->language][0]['value'] = $tweet->entities->media [0]->expanded_url;
  }

  $node->field_tweet_retweet_count[$node->language][0]['value'] = $tweet->retweet_count;

  $node->field_tweet_favorite_count[$node->language][0]['value'] = $tweet->favorite_count;

  $node->field_tweet_creation_date[$node->language][0]['value'] = strtotime($tweet->created_at);

  if (!empty($channel)) {
    $node->field_twitter_channel[$node->language][0]['target_id'] = $channel->nid;
  }

  if (!empty($channel->field_tc_tags[$channel->language][0]['tid'])) {
    $node->field_twitter_tags[$node->language][0]['tid'] = $channel->field_tc_tags[$channel->language][0]['tid'];
  }

  if (sizeof(module_implements('create_tweet_node')) > 0) {
    $data = array('node' => $node, 'tweet' => $tweet);
    $statuses = module_invoke_all('create_tweet_node', $data);
    if (in_array(0, $statuses)) {
      TweetUtils::debug("Twitter node {$tweet->id_str} : '{$node->title}' skipped.");
      return FALSE;
    }
  }

  // Save the node
  node_save($node);

  TweetUtils::debug("Twitter node {$tweet->id_str} : '{$node->title}' created.");

  return $node->nid;
}

/*
 * Publish a given tweet node and also if configured, retweet it to Twitter.
 * Used as a callback for the Ajax publish by curators.
 */
function twitterator_publish_tweet_node($nid) {
  $tweet = node_load($nid);
  $channel_id = $tweet->field_twitter_channel[$tweet->language][0]['target_id'];
  $channel = node_load($channel_id);

  // get the authentication details
  $account_id = $channel->field_twitter_account[$channel->language][0]['target_id'];
  $twitter_account = entity_load_single('twitter_account', $account_id);

  $tweet_id = $tweet->field_tweet_id[$tweet->language][0]['value'];
  $retweet_mode = $channel->field_tc_retweet_mode[$channel->language][0]['value'];
  if ($tweet->status == 0) {
    $tweet->status = 1;
    node_save($tweet);
    if ($retweet_mode == 1) {
      twitterator_retweet_tweet($tweet_id, $twitter_account);
    }
  }
}

/*
 * Get the json status info of a given tweet
 */
function buzztrack_get_tweet_status($tweet_id, $twitter_account) {
  $url = 'https://api.twitter.com/1.1/statuses/show.json';

  // get the authentication details
  $settings = twitterator_twitter_account_info($twitter_account);

  $getfield = '?id=' . $tweet_id;

  $requestMethod = 'GET';
  $twitter = new TwitterAPIExchange($settings);
  $results = $twitter->setGetfield($getfield)
                     ->buildOauth($url, $requestMethod)
                     ->performRequest();

  $data = json_decode($results);

  return $data;
}