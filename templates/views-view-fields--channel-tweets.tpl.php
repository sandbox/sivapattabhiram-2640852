<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>

<div class="tweet-panel tweet-panel-default">
   <div class="tweet-panel-body">
    <div class="tweet-header clearfix">
      <div class="tweet-left">
       <a href="<?php print $fields['field_tweet_user_link']->content; ?>"><img src="<?php print $fields['field_tweet_user_image']->content; ?>"></a>
      </div>
      <div class="tweet-title">
       <div class="tweet-name"><a target="_blank" href="<?php print $fields['field_tweet_user_link']->content; ?>"><?php print $fields['field_tweet_user_name']->content; ?></a></div>
      </div>
      <div class="tweet-title">
       <div class="tweet-sname small"><a target="_blank" href="<?php print $fields['field_tweet_user_link']->content; ?>" rel="nofollow">@<?php print $fields['field_tweet_screen_name']->content; ?></a></div>
      </div>
    </div>
      <?php if (!empty($fields['field_tweet_media_url']->content)): ?>
      <div class="tweet-media">
       <a href="<?php print $fields['field_tweet_media_expanded_url']->content; ?>"><img class="img-responsive" src="<?php print $fields['field_tweet_media_url']->content; ?>">
       </a>
      </div>
      <?php endif; ?>
      <div class="tweet-content mt3">
       <?php print $fields['field_tweet_text']->content; ?>
      </div>
      <div class="tweet-date small">
       <?php $tweet_link = 'https://twitter.com/' .  $fields['field_tweet_screen_name']->content . '/status/' . $fields['field_tweet_id']->content; ?>
       <?php print date('g:i A - j M Y', $fields['field_tweet_creation_date']->content); ?>&nbsp;|&nbsp;
       <?php print $fields['field_tweet_retweet_count']->content + $fields['field_tweet_favorite_count']->content . " mentions"; ?>&nbsp;|&nbsp;
       <a target="_blank" href="<?php print $tweet_link; ?>">View original tweet</a>
      </div>
    </div>
</div>

