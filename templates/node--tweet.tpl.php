<?php

/**
 * @file
 * Twitterator module implementation to display a tweet node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>

<div class="tweet-panel tweet-panel-default">
 <div class="tweet-panel-body">
  <div class="tweet-header clearfix">
    <div class="tweet-left">
     <a href="<?php print $node->field_tweet_user_link['und'][0]['value']; ?>"><img src="<?php print $node->field_tweet_user_image['und'][0]['value']; ?>"></a>
    </div>
    <div class="tweet-title">
     <div class="tweet-name"><a target="_blank" href="<?php print $node->field_tweet_user_link['und'][0]['value']; ?>"><?php print $node->field_tweet_user_name['und'][0]['value']; ?></a></div>
    </div>
    <div class="tweet-title">
     <div class="tweet-sname small"><a target="_blank" href="<?php print $node->field_tweet_user_link['und'][0]['value']; ?>" rel="nofollow">@<?php print $node->field_tweet_screen_name['und'][0]['value']; ?></a></div>
    </div>
  </div>
    <?php if (!empty($node->field_tweet_media_url['und'][0]['value'])): ?>
    <div class="tweet-media">
     <a href="<?php print $node->field_tweet_media_expanded_url['und'][0]['value']; ?>"><img class="img-responsive" src="<?php print $node->field_tweet_media_url['und'][0]['value']; ?>">
     </a>
    </div>
    <?php endif; ?>
    <div class="tweet-content mt3">
     <?php print $node->field_tweet_text['und'][0]['value']; ?>
    </div>
    <div class="tweet-date small">
     <?php $tweet_link = 'https://twitter.com/' .  $node->field_tweet_screen_name['und'][0]['value'] . '/status/' . $node->field_tweet_id['und'][0]['value']; ?>
     <?php print date('g:i A - j M Y', $node->field_tweet_creation_date['und'][0]['value']); ?>&nbsp;|&nbsp;
     <a target="_blank" href="<?php print $tweet_link; ?>"><?php print $node->field_tweet_retweet_count['und'][0]['value'] + $node->field_tweet_favorite_count['und'][0]['value'] . " mentions"; ?></a>
     <?php if ($node->status == 0):?>
       &nbsp;|&nbsp;<a class="publish-node" id="node-pub-<?php print $node->nid;?>" href="#/">Publish</a><span class="ajax-progress ajax-progress-throbber"><span class="throbber" id="throbber-<?php print $node->nid; ?>">&nbsp;</span></span>
     <?php endif;?>
     <?php if ($node->status == 0):?>
     <div class="row">
      <div class="col-md-12 col-xs-12"><span id="publish-message-<?php print $node->nid;?>"></span></div>
     </div>
     <?php endif;?>
    </div>
  </div>
</div>
