<?php

/**
 * Administrative Interface for Twitterator
 *
 */
function twitterator_menu_page() {
  $all_blocks = array();
  $blocks = system_admin_menu_block(
    array('tab_root' => 'admin/config/services/twitterator',
          'path' => 'admin/config/services/twitterator')
  );
  foreach($blocks as $key=>$block) {
    $new_block = $block;
    $new_block['show'] = TRUE;
    $all_blocks[] = $new_block;
  }
  $block_out['content'] = theme('admin_block_content', array('content' => $all_blocks));
  $block_out['title'] = t('Twitterator Configuration');
  $block_out['show'] = TRUE;

  return theme('admin_page', array('blocks' => array($block_out)));
}

/**
 * General Settings Form
 *
 */
function twitterator_settings_form($form, &$form_state) {
  $form['twitterator_time_threshold'] = array(
    '#type' => 'textfield',
    '#title' => t('Time Threshold'),
    '#description' => t('Process only the tweets that are created in the last specified minutes. A value of 0 will ignore this setting.'),
    '#default_value' => variable_get('twitterator_time_threshold', 0),
  );

  $form['twitterator_popular_scheme'] = array(
    '#type' => 'select',
    '#title' => t('Popular Scheme'),
    '#default_value' => variable_get('twitterator_popular_scheme', 1),
    '#options' => array(1 => "Retweets", 2 => "Favorites", 3 => "Retweets + Favorites", 4 => "Custom"),
    '#description' => t('Parameter to determine the popularity of the tweets.')
  );

  $form['twitterator_popular_threshold'] = array(
    '#type' => 'textfield',
    '#title' => t('Popular Threshold'),
    '#description' => t('Process only the tweets that have the minimum specified popular threshold. A value of 0 will ignore this setting.'),
    '#default_value' => variable_get('twitterator_popular_threshold', 0),
  );

  $form['twitterator_log_file'] = array(
    '#type' => 'textfield',
    '#title' => t('Log file'),
    '#description' => t('Log file to capture extended debug information.'),
    '#default_value' => variable_get('twitterator_log_file', '/tmp/twitterator.log'),
  );

  $form['twitterator_use_node_templates'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Twitterator Node Templates'),
    '#description' => t('Use twitterator node templates for displaying tweets and videos. Uncheck this to override display in your themes.'),
    '#default_value' => variable_get('twitterator_use_node_templates', 1),
  );

  return system_settings_form($form);
}
