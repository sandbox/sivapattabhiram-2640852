<?php

mb_internal_encoding("UTF-8");

class TweetUtils {
  static function debug($msg, $show = true) {
    $ts = strftime ( "%H:%M:%S", time () );
    if (function_exists ( 'posix_getpid' )) {
      $ts = "$ts/" . posix_getpid ();
    }

    if ($show && ! (defined('QUIET') && QUIET)) {
      print "[$ts] $msg\n";
    }

    $logfile = variable_get('twitterator_log_file');

    if ($logfile) {
      $fp = fopen($logfile, 'a+');

      if ($fp) {
        $locked = false;

        if (function_exists("flock")) {
          $tries = 0;

          // try to lock logfile for writing
          while ($tries < 5 && ! $locked = flock($fp, LOCK_EX | LOCK_NB)) {
            sleep(1);
            ++ $tries;
          }

          if (!$locked) {
            fclose($fp);
            return;
          }
        }

        fputs($fp, "[$ts] $msg\n");

        if (function_exists("flock")) {
          flock($fp, LOCK_UN);
        }

        fclose($fp);
      }
    }
  } // function _debug

  static function tweet_extract_text($tweet) {
    $hashtag_link_pattern = '<a href="http://twitter.com/search?q=%%23%s&src=hash" rel="nofollow" target="_blank">#%s </a>';
    $url_link_pattern = '<a href="%s" rel="nofollow" target="_blank" title="%s">%s </a>';
    $user_mention_link_pattern = '<a href="http://twitter.com/%s" rel="nofollow" target="_blank" title="%s">@%s </a>';
    $media_link_pattern = '<a href="%s" rel="nofollow" target="_blank" title="%s">%s </a>';

    $text = $tweet->text;
    $entity_holder = array ();

    foreach ( $tweet->entities->hashtags as $hashtag ) {
      $entity = new stdclass ();
      $entity->start = $hashtag->indices [0];
      $entity->end = $hashtag->indices [1];
      $entity->length = $hashtag->indices [1] - $hashtag->indices [0];
      $entity->replace = sprintf ( $hashtag_link_pattern, strtolower ( $hashtag->text ), $hashtag->text );

      $entity_holder [$entity->start] = $entity;
    }

    foreach ( $tweet->entities->urls as $url ) {
      $entity = new stdclass ();
      $entity->start = $url->indices [0];
      $entity->end = $url->indices [1];
      $entity->length = $url->indices [1] - $url->indices [0];
      $entity->replace = sprintf ( $url_link_pattern, $url->url, $url->expanded_url, $url->display_url );

      $entity_holder [$entity->start] = $entity;
    }

    foreach ( $tweet->entities->user_mentions as $user_mention ) {
      $entity = new stdclass ();
      $entity->start = $user_mention->indices [0];
      $entity->end = $user_mention->indices [1];
      $entity->length = $user_mention->indices [1] - $user_mention->indices [0];
      $entity->replace = sprintf ( $user_mention_link_pattern, strtolower ( $user_mention->screen_name ), $user_mention->name, $user_mention->screen_name );

      $entity_holder [$entity->start] = $entity;
    }

    if (isset ( $tweet->entities->media )) {
      foreach ( $tweet->entities->media as $media ) {
        $entity = new stdclass ();
        $entity->start = $media->indices [0];
        $entity->end = $media->indices [1];
        $entity->length = $media->indices [1] - $media->indices [0];
        $entity->replace = sprintf ( $media_link_pattern, $media->url, $media->expanded_url, $media->display_url );

        $entity_holder [$entity->start] = $entity;
      }
    }

    krsort ( $entity_holder );
    foreach ( $entity_holder as $entity ) {
      $text = self::mb_substr_replace ( $text, $entity->replace, $entity->start, $entity->length );
    }

    $text = self::tweet_tidy_text($text);

    return $text;
  }

  static function mb_substr_replace($string, $replacement, $start, $length = 0) {
    if (is_array ( $string )) {
      foreach ( $string as $i => $val ) {
        $repl = is_array ( $replacement ) ? $replacement [$i] : $replacement;
        $st = is_array ( $start ) ? $start [$i] : $start;
        $len = is_array ( $length ) ? $length [$i] : $length;

        $string [$i] = mb_substr_replace ( $val, $repl, $st, $len );
      }

      return $string;
    }

    $result = mb_substr ( $string, 0, $start, 'UTF-8' );
    $result .= $replacement;

    if ($length > 0) {
      $result .= mb_substr ( $string, ($start + $length + 1), mb_strlen ( $string, 'UTF-8' ), 'UTF-8' );
    }

    return $result;
  }

  /**
   * Strip off 4 byte unicode characters from twitter text since mysql crashes
   * while handling tweets/text with japanese smiley characters which are 4 bytes wide
   * best solution for now is to strip off 4 byte unicode characters using reference code from
   * https://www.drupal.org/project/strip_utf8mb4
   */
  static function tweet_tidy_text($text) {
    $text = self::_strip_utf8mb4_for_text_fields ( $text );
    return $text;
  }

  /**
   * Reference code from https://www.drupal.org/project/strip_utf8mb4
   */
  static function _strip_utf8mb4_for_text_fields($text_data, $replace_text ='') {
    $replacements_done = array();
    // Strip overly long 2 byte sequences, as well as characters
    // above U+10000 and replace with $replace_text
    $processed_text_data = preg_replace('/[\x00-\x08\x10\x0B\x0C\x0E-\x19\x7F]' .
      '|[\x00-\x7F][\x80-\xBF]+' .
      '|([\xC0\xC1]|[\xF0-\xFF])[\x80-\xBF]*' .
      '|[\xC2-\xDF]((?![\x80-\xBF])|[\x80-\xBF]{2,})' .
      '|[\xE0-\xEF](([\x80-\xBF](?![\x80-\xBF]))|(?![\x80-\xBF]{2})|[\x80-\xBF]{3,})/S',
      $replace_text, $text_data, -1, $replacements_done[]);

    // Strip overly long 3 byte sequences and UTF-16 surrogates and replace with $replace_text
    $processed_text_data = preg_replace('/\xE0[\x80-\x9F][\x80-\xBF]' .
                                            '|\xED[\xA0-\xBF][\x80-\xBF]/S', $replace_text, $processed_text_data, -1, $replacements_done[]);

    if (array_sum($replacements_done) > 0) {
      $message = t('Unsupported characters in your text were replaced with "!replacement"', array('!replacement' => $replace_text));
      self::debug($message);
    }

    return $processed_text_data;
  }
}
