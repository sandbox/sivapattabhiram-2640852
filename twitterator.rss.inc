<?php

/**
 * @file
 * RSS specific functionality.
 *
 */

/**
 * Update the channel: Fetch the feed, apply filters and execute actions.
 */
function twitterator_process_rss_channel($channel) {
  TweetUtils::debug("Processing channel {$channel->title}");
  $lang = $channel->language;
  $channel_id = $channel->nid;

  // get the time threshold
  $time_threshold = $channel->field_rc_time_threshold[$lang][0]['value'];
  if (empty($time_threshold)) {
    $time_threshold = variable_get('twitterator_time_threshold', 0);
  }

  $channel_items = array();

  // get all the sources
  $sources = $channel->field_rc_source[$lang];
  foreach ($sources as $source) {
    $source_item = entity_load_single('field_collection_item', $source['value']);
    $feed_name = $source_item->field_rc_feed_name[$lang][0]['value'];
    $feed_url = $source_item->field_rc_feed_url[$lang][0]['value'];

    TweetUtils::debug("Processing feed $feed_name");
    $items = twitterator_fetch_rss_feed_simplepie($feed_url);

    if (empty($items)) continue;

    // apply the filters
    foreach($items as $item) {
      $created = $item['timestamp'];
      if (!empty($time_threshold) && $created < (time() - 60 * $time_threshold))
        continue;

      if (sizeof(module_implements('process_rss_item')) > 0) {
        $channel_item = array('channel' => $channel->title, 'feed_name' => $feed_name, 'feed_item' => $item);
        $statuses = module_invoke_all('process_rss_item', $channel_item);
        if (in_array(0, $statuses)) continue;
        $channel_items[] = $channel_item;
      }
    }
  }

  // apply publish actions
  $items_published = array();

  foreach ($channel_items as $channel_item) {
    $feed_name = $channel_item['feed_name'];
    $feed_item = $channel_item['feed_item'];

    //TweetUtils::debug("Processing feed item {$feed_item['url']} from $feed_name.");

    // check if the item is already created in the channel
    $nid = twitterator_get_rss_item($feed_item['url'], $channel_id);
    if (empty($nid)) {
      twitterator_create_rss_item($feed_item, $channel, TRUE);
    }
  }
}

/**
 * Parse the rss feed using simplepie parser.
 */
function twitterator_fetch_rss_feed_simplepie($url) {
  module_load_include('inc', 'twitterator', 'inc/http_request');
  module_load_include('php', 'twitterator', 'inc/simplepie.compiled');

  // reference: feeds/plugins/FeedsSimplePieParser.inc and feeds/plugins/FeedsHTTPFetcher.inc
  // Please be quiet SimplePie.
  $level = error_reporting();
  error_reporting($level ^ E_DEPRECATED ^ E_STRICT);

  // fetch the feed using http_request library
  try {
    $result = http_request_get($url, NULL, NULL, NULL, 60);
  }
  catch (HRCurlException $e) {
    watchdog('twitterator', "Download of @url failed: !message", array('@url' => $url, '!message' => $e->getMessage()), WATCHDOG_ERROR);
    TweetUtils::debug(t('Download of @url failed: !message', array('@url' => $url, '!message' => $e->getMessage())));
    return FALSE;
  }

  if (!in_array($result->code, array(200, 201, 202, 203, 204, 205, 206))) {
    watchdog("twitterator", 'Download of @url failed with code !code.', array('@url' => $url, '!code' => $result->code), WATCHDOG_ERROR);
    TweetUtils::debug(t('Download of @url failed with code !code.', array('@url' => $url, '!code' => $result->code)));
    return FALSE;
  }

  // Initialize SimplePie.
  $parser = new SimplePie();
  $parser->set_raw_data($result->data);
  $parser->set_stupidly_fast(TRUE);
  $parser->encode_instead_of_strip(FALSE);
  // @todo Is caching effective when we pass in raw data?
  $parser->enable_cache(TRUE);
  $directory = 'public://simplepie';
  file_prepare_directory($dir, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
  $parser->set_cache_location($directory);
  if (!$parser->init()) {
    TweetUtils::debug("Failed to parse the feed $url");
    return FALSE;
  }

  // Construct the standard form of the parsed feed
  $feed_title = html_entity_decode(($title = $parser->get_title()) ? $title : twitterator_create_title($parser->get_description()));
  $feed_description = $parser->get_description();
  $feed_link = html_entity_decode($parser->get_link());

  $items = array();
  $items_num = $parser->get_item_quantity();
  for ($i = 0; $i < $items_num; $i++) {
    $simplepie_item = $parser->get_item($i);
    $item = array();
    $item['title'] = strip_tags(html_entity_decode($simplepie_item->get_title()));
    $item['description'] = $simplepie_item->get_content();
    $item['url'] = html_entity_decode($simplepie_item->get_link());
    // Use UNIX time. If no date is defined, fall back to REQUEST_TIME.
    $item['timestamp'] = $simplepie_item->get_date('U');
    if (empty($item['timestamp'])) {
      $item['timestamp'] = REQUEST_TIME;
    }
    $item['guid'] = $simplepie_item->get_id();
    // Use URL as GUID if there is no GUID.
    if (empty($item['guid'])) {
      $item['guid'] = $item['url'];
    }
    $author = $simplepie_item->get_author();
    $item['author_name'] = isset($author->name) ? html_entity_decode($author->name) : '';
    $item['author_link'] = isset($author->link) ? $author->link : '';
    $item['author_email'] = isset($author->email) ? $author->email : '';

    $item['raw'] = $simplepie_item->data;
    $items[] = $item;
  }

  // Release parser.
  unset($parser);
  // Set error reporting back to its previous value.
  error_reporting($level);

  return $items;
}

/**
 * Fetch a Feed Item node from a channel given a url.
 */
function twitterator_get_rss_item($url, $channel_id) {
  $query = new EntityFieldQuery();
  $result = $query->entityCondition('entity_type', 'node')
                  ->entityCondition('bundle', 'rss_item')
                  ->fieldCondition('field_rss_item_url', 'value', $url, '=')
                  ->fieldCondition('field_rss_channel', 'target_id', $channel_id, '=')
                  ->execute();
  if (isset($result['node'])) {
    $rss_items_nids = array_keys($result['node']);
    return array_pop($rss_items_nids);
  }

  return 0;
}

/**
 * Create a RSS Item node object from the simplepie rss item object.
 * @param object $feed_item
 *   The rss item returned by the simplepie
 * @param int channel_id
 *   The node id of the channel to which this item has to be added
 * @return int node_id
 *   The node id of the created feed item.
 */
function twitterator_create_rss_item($rss_item, $channel, $publish=TRUE) {
  $node = new stdClass();

  $node->type = 'rss_item';
  $node->uid = 1;
  $node->status = $publish;
  $node->comment = 0;
  $node->promote = 0;
  $node->moderate = 0;
  $node->sticky = 0;
  $node->language = LANGUAGE_NONE;

  $node->title = $rss_item['title'];

  $node->field_rss_item_description[$node->language][0] = array(
    'value' => html_entity_decode($rss_item['description'], ENT_QUOTES, "utf-8"),
    //'value' => utf8_encode(htmlspecialchars_decode($content)),
    'format' => 'full_html',
  );

  $node->field_rss_item_url[$node->language][0]['value'] = $rss_item['url'];

  $node->field_rss_item_published_date[$node->language][0]['value'] = $rss_item['timestamp'];

  if (!empty($channel)) {
    $node->field_rss_channel[$node->language][0]['target_id'] = $channel->nid;
  }

  if (!empty($channel->field_rc_tags[$channel->language][0]['tid'])) {
    $node->field_rss_item_tags[$node->language][0]['tid'] = $channel->field_rc_tags[$channel->language][0]['tid'];
  }

  if (sizeof(module_implements('create_rss_item_node')) > 0) {
    $data = array('node' => $node, 'rss_item' => $rss_item);
    $statuses = module_invoke_all('create_rss_item_node', $data);
    if (in_array(0, $statuses)) {
      TweetUtils::debug("RSS Item node: '{$node->title}' skipped.");
      return FALSE;
    }
  }

  // Save the node
  node_save($node);

  TweetUtils::debug("RSS Item node: '{$node->title}' created.");

  return $node->nid;
}
