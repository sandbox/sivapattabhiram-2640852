<?php

/**
 * @file
 * Documentation for twitterator API.
 *
 */

/**
 * Determine if the tweet should be processed or not.
 *
 * @param object $tweet
 *   The Tweet object constructed from the json returned by the Twitter api.
 * @return boolean status
 *   A boolean flag - if set to true, the tweet will not be processed by twitterator .
 */
function hook_tweet_filter($tweet) {
  TweetUtils::debug("hook_filter_tweets: Processing tweet {$tweet->id_str}");
  return FALSE;
}

/**
 * Alter Tweet item node before saving.
 *
 * @param object $node
 *   The Tweet item node object that is being created.
 * @param object $tweet
 *   The Tweet object constructed from the json returned by the Twitter api.
 */
function hook_tweet_node_save_alter(&$node, &$tweet) {
  TweetUtils::debug("hook_tweet_node_alter: Processing tweet {$tweet->id_str}");
}
