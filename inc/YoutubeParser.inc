<?php

/**
 * @file
 * RSS/Atom feed parser class for Youtube based on SimpleXML
 * Based on the Feeds Youtube Parser (https://www.drupal.org/project/feeds_youtube)
 * adapted to work independently without the Feeds module
 */

/**
 * Class definition for Youtube Parser.
 *
 * Parses RSS or Atom feeds returned from YouTube Channel Feeds.
 */
class YoutubeParser {

  public function parse($data) {

    if (!defined('LIBXML_VERSION') || (version_compare(phpversion(), '5.1.0', '<'))) {
      @$sxml = simplexml_load_string($data, NULL);
    }
    else {
      @$sxml = simplexml_load_string($data, NULL, LIBXML_NOERROR | LIBXML_NOWARNING | LIBXML_NOCDATA);
    }

    // Got a malformed XML.
    if ($sxml === FALSE || is_null($sxml)) {
      watchdog('twitterator', t('YoutubeParser: Malformed XML source.'));
      return NULL;
    }

    // Run parsing if the feed is Atom or RSS
    if ($this->isAtomFeed($sxml)) {
      $result = $this->parseAtom($sxml);

    }
    elseif ($this->isRssFeed($sxml)) {
      $result = $this->parseRss20($sxml);

    }
    else {
      watchdog('twitterator', t('YoutubeParser: Unknown type of feed.'));
      return NULL;
    }
    return $result;
  }

  /**
   * Check if given feed object is an Atom feed.
   *
   * @param SimpleXMLElement $sxml
   *
   * @return boolen
   *   TRUE if given SimpleXML object is Atom feed or FALSE
   */
  protected function isAtomFeed(SimpleXMLElement $sxml) {
    return $sxml->getName() == 'feed';
  }

  /**
   * Check if given feed object is a RSS feed.
   *
   * @param SimpleXMLElement $sxml
   *
   * @return boolen
   *   TRUE if given SimpleXML object is RSS feed or FALSE
   */
  protected function isRssFeed(SimpleXMLElement $sxml) {
    return $sxml->getName() == 'rss';
  }

  /**
   *  Display seconds as HH:MM:SS, with leading 0's.
   *
   *  @param $seconds
   *    The number of seconds to display.
   */
  public function secsToTime($seconds) {
    // Number of seconds in an hour.
    $unith =3600;
    // Number of seconds in a minute.
    $unitm =60;

    // '/' given value by num sec in hour... output = HOURS
    $hh = intval($seconds / $unith);

    // Multiply number of hours by seconds, then subtract from given value.
    // Output = REMAINING seconds.
    $ss_remaining = ($seconds - ($hh * 3600));

    // Take remaining seconds and divide by seconds in a min... output = MINS.
    $mm = intval($ss_remaining / $unitm);
    // Multiply number of mins by seconds, then subtract from remaining seconds.
    // Output = REMAINING seconds.
    $ss = ($ss_remaining - ($mm * 60));

    $output = '';

    // If we have any hours, then prepend that to our output.
    if ($hh) {
      $output .= "$hh:";
    }

    // Create a safe-for-output MM:SS.
    $output .= check_plain(sprintf($hh ? "%02d:%02d" : "%d:%02d", $mm, $ss));

    return $output;
  }


  /**
   * Parse Atom feed
   *
   * @param SimpleXMLElement $sxml
   */
  private function parseAtom(SimpleXMLElement $sxml) {

    $result = array();
    $feed_title = (string) $sxml->title;

    // Iterate over entries in feed
    // TODO: This is not DRY - extract things which is same in Atom and RSS20 to common method
    foreach ($sxml->entry as $entry) {
      // get video ID
      $arr = explode('/', $entry->id);
      $id = $arr[count($arr)-1];

      // get the video link
      $attrs = $entry->link->attributes();
      $link = $attrs['href'];

      // get nodes in media: namespace for media information
      $media = $entry->children('http://search.yahoo.com/mrss/');

      // get video player URL
      if ($media->group->player) {
        $attrs = $media->group->player->attributes();
        $watch = str_replace('&feature=youtube_gdata_player', '', $attrs['url']);
      }

      // get video thumbnail
      $attrs = $media->group->thumbnail[0]->attributes();
      $thumbnail = (string) $attrs['url'];

      // get the views and rating from group:community
      if ($media->group->community->starRating) {
        $attrs = $media->group->community->starRating->attributes();
        $favCount = $attrs['count'];
        $rating = $attrs['average'];
      }
      if ($media->group->community->statistics) {
        $attrs = $media->group->community->statistics->attributes();
        $viewCount = $attrs['views'];
      }

      // get <yt:duration> node for video length
      $yt = $media->children('http://gdata.youtube.com/schemas/2007');
      if ($yt->duration) {
        $attrs = $yt->duration->attributes();
        $length = $attrs['seconds'];
      }

      // get <yt:stats> node for viewer statistics
      $yt = $entry->children('http://gdata.youtube.com/schemas/2007');
      if ($yt->statistics) {
        $attrs = $yt->statistics->attributes();
        $viewCount = $attrs['viewCount'];
        $favCount = $attrs['favoriteCount'];
      }

      // get <gd:rating> node for video ratings
      $gd = $entry->children('http://schemas.google.com/g/2005');
      $rating = 0;
      if ($gd->rating) {
        $attrs = $gd->rating->attributes();
        $rating = $attrs['average'];
      }

      $updated = (string) $entry->updated;
      $published = (string) $entry->published;

      $item = array(
        'feed_title' => $feed_title,
        'guid' => (string) $entry->id,
        'video_id' => $id,
        'url' => (string) $link,
        'watch_page' => $watch,
        'title' => (string) $media->group->title,
        'author' => (string) $entry->author->name,
        'description' => (string) $media->group->description,
        'thumbnail' => $thumbnail,
        'category' => (string) $media->group->category,
        'tags' => explode(',', $media->group->keywords),
        'embedded_player' => '',
        'duration' => $this->secsToTime($length),
        'duration_raw' => $length,
        'view_count' => (string) $viewCount,
        'fav_count' => (string) $favCount,
        'rating' => (string) $rating,
        'updated_datetime' => date('Y-m-d H:i:s', strtotime($updated)),
        'updated_timestamp' => strtotime($updated),
        'published_datetime' => date('Y-m-d H:i:s', strtotime($published)),
        'published_timestamp' => strtotime($published),
      );

      $result[] = $item;
    }
    return $result;
  }

  /**
   * Parse RSS 2.0 feed
   *
   * @param SimpleXMLElement $sxml
   */
  private function parseRss20(SimpleXMLElement $sxml) {

    // XML was parsed successfully, so we can begin to process items
    $result = array();
    $feed_title = (string) $sxml->title;

    // Iterate over entries in feed
    // TODO: This is not DRY - extract things which is same in Atom and RSS20 to common method
    foreach ($sxml->xpath('//item') as $entry) {

      // Get atom nodes
      $atom = $entry->children('http://www.w3.org/2005/Atom');
      $updated = $atom->updated;

      // Get video ID
      $id = end(explode('/', $entry->guid));

      // get the video link
      $attrs = $entry->link->attributes;
      $link = $attrs['href'];

      // Get nodes in media: namespace for media information
      $media = $entry->children('http://search.yahoo.com/mrss/');

      // Get video player URL
      if ($media->group->player) {
        $attrs = $media->group->player->attributes();
        $player = (string) $attrs['url'];
      }

      // Get video thumbnail
      $attrs = $media->group->thumbnail[0]->attributes();
      $thumbnail = (string) $attrs['url'];

      // get the views and rating from group:community
      if ($media->group->community->starRating) {
        $attrs = $media->group->community->starRating->attributes();
        $favCount = $attrs['count'];
        $rating = $attrs['average'];
      }
      if ($media->group->community->statistics) {
        $attrs = $media->group->community->statistics->attributes();
        $viewCount = $attrs['views'];
      }

      // Get <yt:duration> node for video length
      $yt = $media->children('http://gdata.youtube.com/schemas/2007');
      if ($yt->duration) {
        $attrs = $yt->duration->attributes();
        $length = (int) $attrs['seconds'];
      }

      // Get <yt:stats> node for viewer statistics
      $yt = $entry->children('http://gdata.youtube.com/schemas/2007');
      if ($yt->statistics) {
        $attrs = $yt->statistics->attributes();
        $viewCount = (int) $attrs['viewCount'];
        $favCount = (int) $attrs['favoriteCount'];
      }

      // Get <gd:rating> node for video ratings
      $gd = $entry->children('http://schemas.google.com/g/2005');
      $rating = 0;
      if ($gd->rating) {
        $attrs = $gd->rating->attributes();
        $rating = (int) $attrs['average'];
      }

      $item = array(
        'feed_title' => $feed_title,
        'guid' => (string) $entry->guid,
        'video_id' => $id,
        'url' => (string) $link,
        'watch_page' => 'http://www.youtube.com/watch?v=' . $id,
        'title' => html_entity_decode((string) $media->group->title),
        'author' => (string) $entry->author,
        'description' => html_entity_decode((string) $media->group->description),
        'thumbnail' => $thumbnail,
        'category' => (string) $media->group->category,
        'tags' => explode(',', (string) $media->group->keywords),
        'embedded_player' => $player,
        'duration' => $this->secsToTime($length),
        'duration_raw' => $length,
        'view_count' => $viewCount,
        'fav_count' => $favCount,
        'rating' => $rating,
        'updated_datetime' => date('Y-m-d H:i:s', strtotime($updated)),
        'updated_timestamp' => strtotime($updated),
        'published_datetime' => date('Y-m-d H:i:s', strtotime($entry->pubDate)),
        'published_timestamp' => strtotime($entry->pubDate),
      );

      // Populate the FeedsFetcherResult object with the parsed results.
      $result[] = $item;
    }
    return $result;
  }

}
